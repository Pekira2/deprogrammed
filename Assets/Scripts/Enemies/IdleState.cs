using UnityEngine;
using System.Collections;

public class IdleState : IEnemyState {

	private Enemy enemy;
	private float idleTimer;
	private float idleDurationMin = 3;
    private float idleDurationMax = 12;
    float idleDuration;
    float randomFlip;

    public void Execute(){
    	Idle();
        if (enemy.Target != null)
        {
            enemy.ChangeState(new PatrolState());
        }
    }

    public void Enter(Enemy enemy){
    		this.enemy = enemy;
            idleDuration = Random.Range(idleDurationMin, idleDurationMax);

    }

    public void Exit(){


    }

    public void OnTriggerEnter(Collider2D other){


    }

    void Idle()
    {
    	enemy.anim.SetFloat("xVel", 0);
    	enemy.anim.SetBool("isGrounded", true);
    	enemy.anim.SetBool("isDown", false);

        enemy.GetComponent<Rigidbody2D>().velocity = new Vector2(0 , enemy.GetComponent<Rigidbody2D>().velocity.y);

    	idleTimer += Time.deltaTime;

    	if (idleTimer >= idleDuration)
    	{
    		
            randomFlip = Random.Range(-1.0f, 1.0f);
            if (randomFlip>0)
            {
                enemy.Flip();
            } 
            enemy.ChangeState(new PatrolState());
    	}
    }


}
