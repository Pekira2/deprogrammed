using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public interface IEnemyState {

	void Execute();
	void Enter(Enemy enemy);
	void Exit();
	void OnTriggerEnter(Collider2D other);


}