using UnityEngine;
using System.Collections;

public class PatrolState : IEnemyState {

    private Enemy enemy;
    private float patrolTimer;
    private float patrolDurationMin = 7;
    private float patrolDurationMax = 14;
    float patrolDuration;
    float boolForJump;

    public void Execute(){
        Patrol();
        enemy.Move();
        if (enemy.Target != null)
        {
            enemy.ChangeState(new RangedState());
        }
    }

    public void Enter(Enemy enemy){
        this.enemy = enemy;
        patrolDuration = Random.Range(patrolDurationMin, patrolDurationMax);
    }

    public void Exit(){


    }

    public void OnTriggerEnter(Collider2D other){
        if (other.tag == "FlipEdge")
        {
            enemy.anim.SetFloat("xVel", -enemy.anim.GetFloat("xVel"));
            Debug.Log("entra FlipEdge");
            enemy.Flip();
        } else if (other.tag == "JumpEdgeRight" && enemy.facingRight)
        {
            boolForJump = Random.Range(0.0f, 1.0f);
            if (boolForJump>0) 
            {
            	Debug.Log("entra JumpEdgeRight");
            	enemy.Jump();
            }
        } else if (other.tag == "JumpEdgeLeft" && !enemy.facingRight)
        {
            boolForJump = Random.Range(0.0f, 1.0f);
            if (boolForJump>0) 
            {
            	Debug.Log("entra JumpEdgeLeft");
            	enemy.Jump();
            }
        }

    }

    void Patrol()
    {
        enemy.anim.SetBool("isGrounded", enemy.IsGrounded());
        if (enemy.IsGrounded())
        {   
            enemy.anim.SetFloat("xVel", enemy.GetComponent<Enemy>().maxSpeedWalk);
        }
        
        enemy.anim.SetBool("isDown", false);
        enemy.anim.SetBool("isDead", false);

        patrolTimer += Time.deltaTime;

        if (patrolTimer >= patrolDuration)
        {
            enemy.ChangeState(new IdleState());
        }

    }

}
