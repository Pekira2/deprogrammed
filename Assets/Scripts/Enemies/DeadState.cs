using UnityEngine;
using System.Collections;

public class DeadState : IEnemyState {

	private Enemy enemy;

    public void Execute(){
    	Dead();
    }

    public void Enter(Enemy enemy){
    		this.enemy = enemy;
    }

    public void Exit(){
    		
    }

    public void OnTriggerEnter(Collider2D other){
        if (other.tag == "Player" && !enemy.GetComponent<Enemy>().isVoid)
        {
            other.GetComponent<playerController>().ammo += enemy.GetComponent<Enemy>().extraAmmo;
            enemy.GetComponent<Enemy>().isVoid = true;
        }
    }

    void Dead()
    {
    	enemy.anim.SetBool("isDead", true);
        enemy.GetComponent<Rigidbody2D>().velocity = new Vector2(0 , 0);
    }


}
