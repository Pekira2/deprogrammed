using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playerController : MonoBehaviour {

    public Transform shootTransform;

    
    public float maxSpeedRotate = 10f;
    public float maxSpeedWalk = 5f;
    float maxSpeedWalk2;
    bool facingRight = true;
    

    Animator anim;

    bool grounded = true;
    bool down = false;
    bool climbing = false;
    

    public Transform groundCheck;

    public LayerMask whatIsGround;
    public float groundRadius = 0.3f;

    public LayerMask whatCanBeClimbed;
    public float climbRadius = 0.3f;

    public float jumpForce = 700f;
    int jumpCounter = 0;
    public int maxJumpCounter = 2;

    public float climbForce = 350f;
    int climbCounter = 0;
    public int maxClimbCounter = 2;
    
    float xVel;
    float yVel;

    //public Transform groundCheckPlayer;
    public AudioSource jumpSound;

    public GameObject shootPrefab;

    public int lifes = 3;
    bool isDead;

    public int initialAmmo = 100;
    public int ammo;

    public LayerMask layerEnemy;


    GameObject cloneBackObj;

    void Start()
    {
        anim = this.GetComponent<Animator>();    
        ammo = initialAmmo;  
    }


    void Update()
    {
        if (lifes == 0) isDead = true;

        if (!isDead)
        {
            if (Input.GetKey(KeyCode.LeftShift))
            {
                maxSpeedWalk2 = maxSpeedWalk*2f;
            } else
            {
                maxSpeedWalk2 = maxSpeedWalk;
            }

            if (!down)
            {
                xVel = Input.GetAxis("Horizontal") * maxSpeedWalk2;
            } else
                xVel = Input.GetAxis("Horizontal") * maxSpeedRotate;
             
            grounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
            if (grounded) jumpCounter=0;

            climbing = canClimb();
            if (!climbing) climbCounter=0;
          
            GetComponent<Rigidbody2D>().velocity = new Vector2(xVel , GetComponent<Rigidbody2D>().velocity.y);
            
            if(Input.GetKeyDown(KeyCode.W) && !climbing)
            {
                if (!down && grounded) {
                    if (jumpCounter < maxJumpCounter)
                    {
                        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, jumpForce));
                        jumpSound.Play();
                    }
                } else
                    down = !down;
            }

            if(Input.GetKeyDown(KeyCode.W) && climbing )
            {
                if (climbCounter < maxClimbCounter)
                    GetComponent<Rigidbody2D>().AddForce(new Vector2(0, climbForce));
            }

            if( Input.GetKeyDown(KeyCode.S) && grounded)
            {
                down = true;
            }

            if (Input.GetMouseButtonDown (0)) {
                Shoot();
            }

            if (xVel > 0 && !facingRight)
                Flip();
            else if (xVel < 0 && facingRight)
                Flip();
                       
        } else
        {
            xVel = 0;
            GetComponent<Rigidbody2D>().velocity = new Vector2(0 , 0);
            Invoke("LoadScene", 5);
        }

        anim.SetFloat("xVel", Mathf.Abs(xVel));  
        anim.SetBool("isGrounded", grounded);
        anim.SetBool("isDown", down);
        anim.SetBool("isDead", isDead);
        anim.SetBool("isGrapping", climbing);
      
    }


    void Flip ()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void Shoot ()
    {
        if (ammo>0)
        {
            int xOff;
            if (facingRight) xOff = 2;
            else xOff = -2;
            GameObject shoot = Instantiate(shootPrefab, new Vector3(shootTransform.position.x, shootTransform.position.y, shootTransform.position.z), Quaternion.identity) as GameObject;
            shoot.GetComponent<shootController>().facingRight = facingRight;
            if (!facingRight) shoot.transform.localScale *= -1;
            ammo-=1;
            hit();
        }
    }

    void hit()
    {
     
        Vector3 targetPosition;
        if (facingRight) targetPosition = new Vector3 (transform.position.x+10, transform.position.y, transform.position.z);
        else             targetPosition = new Vector3 (transform.position.x-10, transform.position.y, transform.position.z);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, targetPosition - transform.position,
            Vector2.Distance(targetPosition, transform.position), layerEnemy);//layer 12 = Player from my transform in the direction to the player as seen in the debug ray
     
        if (hit.collider != null)//if the object that was hit goes by the name "Player" then:
        {
            hit.transform.gameObject.GetComponent<Enemy>().ApplyDamage(1);  
            hit.transform.gameObject.GetComponent<Enemy>().Target = transform.gameObject;  
        }
    }

    bool canClimb()
    {
        Vector3 targetPosition;
        if (facingRight) targetPosition = new Vector3 (shootTransform.position.x+climbRadius, shootTransform.position.y, shootTransform.position.z);
        else             targetPosition = new Vector3 (shootTransform.position.x-climbRadius, shootTransform.position.y, shootTransform.position.z);

        RaycastHit2D hit = Physics2D.Raycast(shootTransform.position, targetPosition - shootTransform.position,
            Vector2.Distance(targetPosition, shootTransform.position), whatCanBeClimbed);
     
        return (hit.collider != null);
    }

    public void ApplyDamage(int damage)
    {
        if (lifes>0) lifes = lifes - damage;
    }

    void LoadScene ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
