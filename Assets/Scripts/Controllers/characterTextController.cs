using UnityEngine;
using System;
using System.Collections;

public class characterTextController : MonoBehaviour {

  public GameObject character;

    void FixedUpdate()
    {
      if (character!=null)
      {
        transform.position = new Vector3(character.transform.position.x, character.transform.position.y+0.3f, transform.position.z);

        if (character.tag == "Enemy")
        {
            if (!character.GetComponent<Enemy>().isDead)
              transform.GetComponent<TextMesh>().text = string.Concat(character.GetComponent<Enemy>().lifes);
            else if (!character.GetComponent<Enemy>().isVoid)
              transform.GetComponent<TextMesh>().text = string.Concat("+");
            
            if (character.GetComponent<Enemy>().isDead && character.GetComponent<Enemy>().isVoid)
            {
              this.gameObject.SetActive(false);
            }
        }

        if (character.tag == "Player")
        {
            transform.GetComponent<TextMesh>().text = string.Concat(character.GetComponent<playerController>().lifes+"/"+character.GetComponent<playerController>().ammo);
        }        

      }
    } 

}
