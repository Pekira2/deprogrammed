using UnityEngine;
using System.Collections;

public class objController : MonoBehaviour {

    public float xVel = 1.0f;
    public float xIni = 1.0f;
   
    public Transform player;
    //public float playerRadius = 2.0f;
    public LayerMask whatIsPlayer;
    public Transform groundCheckPlayer;

    Vector2 pointA;
    Vector2 pointB;

    void Start()
    {
        xIni = GetComponent<Transform>().position.x;
    }
    

    void FixedUpdate()
    {

        if (!GameManager.gameManager.pause) {

            Time.timeScale=1; 

            if (gameObject.tag == "car")

            {

            } else if (gameObject.tag == "ropePiece")

            {
            
                if (groundCheckPlayer != null) {
                
                    pointA.x = transform.position.x - 0.5f;
                    pointA.x = transform.position.y;
                    pointB.x = transform.position.x + 0.5f; // sum the size of the axis X of the box collider
                    pointB.y = transform.position.y + 0.5f; // sum the size of the axis Y of the collider

                    if (Physics2D.OverlapArea(pointA, pointB, whatIsPlayer) && 
                        !Physics2D.OverlapCircle(groundCheckPlayer.position, player.GetComponent<playerController>().groundRadius, player.GetComponent<playerController>().whatIsGround))              
                        {    
                            GetComponent<Rigidbody2D>().AddForce(new Vector2(Input.GetAxis("Horizontal")*0.50f, 0));
                        }
                }
            }
        } else Time.timeScale=0; 
    }


    void Update()
    {

    if (!GameManager.gameManager.pause) {
    
        if (gameObject.tag == "car")

        {
            if (groundCheckPlayer != null) {

                GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().position.x + xVel, GetComponent<Transform>().position.y, GetComponent<Transform>().position.z);  
        
                if ( (  Mathf.Abs( GetComponent<Transform>().position.x - groundCheckPlayer.GetComponent<Transform>().position.x)
                                    <= GetComponent<BoxCollider2D>().size.x )  &&
                    (  Mathf.Abs( GetComponent<Transform>().position.y - groundCheckPlayer.GetComponent<Transform>().position.y)
                                    <= GetComponent<BoxCollider2D>().size.y ) )               
                    player.GetComponent<Transform>().parent = GetComponent<Transform>();
            } 

        } else if (gameObject.tag == "ropePiece")

        {
            if (groundCheckPlayer != null) {
                
                pointA.x = transform.position.x - 0.5f;
                pointA.x = transform.position.y;
                pointB.x = transform.position.x + 0.5f; // sum the size of the axis X of the box collider
                pointB.y = transform.position.y + 0.5f; // sum the size of the axis Y of the collider

                if (Physics2D.OverlapArea(pointA, pointB, whatIsPlayer) && 
                    !Physics2D.OverlapCircle(groundCheckPlayer.position, player.GetComponent<playerController>().groundRadius, player.GetComponent<playerController>().whatIsGround))              
                    { 
                        //transform.parent = player.GetComponent<Transform>();
                        //player.GetComponent<Transform>().position = new Vector2(transform.position.x, transform.position.y) ;
                    }
                }
            }
        }
        else if (GetComponent<Animation>()!=null) GetComponent<Animation>().Stop();

    }


}
