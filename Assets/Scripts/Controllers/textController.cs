using UnityEngine;
using System.Collections;

public class textController : MonoBehaviour {

    public string textId;
    public bool fixedUpdate = false;
    public int lineLenght = 0;

    void Start()
    {
        LoadText();
    } 

    void FixedUpdate()
    {
        if (fixedUpdate) LoadText();
    } 

    void LoadText()
    { 
        if (GameManager.gameManager.textCollection!=null)
        foreach (TextObj textObj in GameManager.gameManager.textCollection.Texts) {
            if (textObj.id==textId) {
                if (lineLenght == 0)
                    transform.GetComponent<TextMesh>().text = textObj.text;
                else
                    transform.GetComponent<TextMesh>().text = ResolveTextSize(textObj.text, lineLenght);
                break;
            }
        } else Debug.Log("GameManager.gameManager.textCollection==null");
    } 

    // Wrap text by line height
    private string ResolveTextSize(string input, int lineLength){
       
       // Split string by char " "         
       string[] words = input.Split(" "[0]);
       //Debug.Log("input="+input+"  wordsNumber="+words.Length);
     
       // Prepare result
       string result = "";
     
       // Temp line string
       string line = "";
     
       // for each all words        
       foreach(string s in words){
         // Append current word into line
         string temp = line + " " + s;
     
         // If line length is bigger than lineLength
         if(temp.Length > lineLength){
     
           // Append current line into result
           result += line + "\n";
           // Remain word append into new line
           line = s;
         }
         // Append current word into current line
         else {
           line = temp;
         }
       }
     
        // Append last line into result        
        result += line;
     
        // Remove first " " char
        //Debug.Log(result.Substring(1,result.Length-1));
        return result.Substring(1,result.Length-1);
     }
}
