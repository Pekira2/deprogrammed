using UnityEngine;
using System.Collections;
using System;

public class playerFollower : MonoBehaviour
{

    GameObject character;
    public string tag;
    public float xOffset = 0f;
    public float yOffset = 0f;


    void Update()
    {
        if (!GameManager.gameManager.pause) {
            if ( character != null) {
                if (Math.Abs(character.transform.position.y - transform.position.y) > yOffset)
                {
                
                    transform.position = new Vector3(character.transform.position.x + xOffset, character.transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(character.transform.position.x + xOffset, transform.position.y, transform.position.z);
                }
            } else character = GameObject.FindGameObjectWithTag(tag);
        }
    }
}