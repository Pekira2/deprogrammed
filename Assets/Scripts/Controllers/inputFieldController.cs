using UnityEngine;
using System.Collections;

public class inputFieldController : MonoBehaviour {

   public GameObject text;

   void Start()
    {  
        text.GetComponent<TextMesh>().text = "|";
    }

    void Update()
    {
            text.GetComponent<TextMesh>().text = text.GetComponent<TextMesh>().text.Replace("|", "");

            if     (Input.GetKeyDown(KeyCode.A)) IntroChar("A");
            else if(Input.GetKeyDown(KeyCode.B)) IntroChar("B");
            else if(Input.GetKeyDown(KeyCode.C)) IntroChar("C");
            else if(Input.GetKeyDown(KeyCode.D)) IntroChar("D");
            else if(Input.GetKeyDown(KeyCode.E)) IntroChar("E");
            else if(Input.GetKeyDown(KeyCode.F)) IntroChar("F");
            else if(Input.GetKeyDown(KeyCode.G)) IntroChar("G");
            else if(Input.GetKeyDown(KeyCode.H)) IntroChar("H");
            else if(Input.GetKeyDown(KeyCode.I)) IntroChar("I");
            else if(Input.GetKeyDown(KeyCode.J)) IntroChar("J");
            else if(Input.GetKeyDown(KeyCode.K)) IntroChar("K");
            else if(Input.GetKeyDown(KeyCode.L)) IntroChar("L");
            else if(Input.GetKeyDown(KeyCode.M)) IntroChar("M");
            else if(Input.GetKeyDown(KeyCode.N)) IntroChar("N");
            else if(Input.GetKeyDown(KeyCode.O)) IntroChar("O");
            else if(Input.GetKeyDown(KeyCode.P)) IntroChar("P");
            else if(Input.GetKeyDown(KeyCode.Q)) IntroChar("Q");
            else if(Input.GetKeyDown(KeyCode.R)) IntroChar("R");
            else if(Input.GetKeyDown(KeyCode.S)) IntroChar("S");
            else if(Input.GetKeyDown(KeyCode.T)) IntroChar("T");
            else if(Input.GetKeyDown(KeyCode.U)) IntroChar("U");
            else if(Input.GetKeyDown(KeyCode.V)) IntroChar("V");
            else if(Input.GetKeyDown(KeyCode.X)) IntroChar("X");
            else if(Input.GetKeyDown(KeyCode.Y)) IntroChar("Y");
            else if(Input.GetKeyDown(KeyCode.Z)) IntroChar("Z");
            else if(Input.GetKeyDown(KeyCode.Alpha0)) IntroChar("0");
            else if(Input.GetKeyDown(KeyCode.Alpha1)) IntroChar("1");
            else if(Input.GetKeyDown(KeyCode.Alpha2)) IntroChar("2");
            else if(Input.GetKeyDown(KeyCode.Alpha3)) IntroChar("3");
            else if(Input.GetKeyDown(KeyCode.Alpha4)) IntroChar("4");
            else if(Input.GetKeyDown(KeyCode.Alpha5)) IntroChar("5");
            else if(Input.GetKeyDown(KeyCode.Alpha6)) IntroChar("6");
            else if(Input.GetKeyDown(KeyCode.Alpha7)) IntroChar("7");
            else if(Input.GetKeyDown(KeyCode.Alpha8)) IntroChar("8");
            else if(Input.GetKeyDown(KeyCode.Alpha9)) IntroChar("9");
            else if(Input.GetKeyDown(KeyCode.Keypad0)) IntroChar("0");
            else if(Input.GetKeyDown(KeyCode.Keypad1)) IntroChar("1");
            else if(Input.GetKeyDown(KeyCode.Keypad2)) IntroChar("2");
            else if(Input.GetKeyDown(KeyCode.Keypad3)) IntroChar("3");
            else if(Input.GetKeyDown(KeyCode.Keypad4)) IntroChar("4");
            else if(Input.GetKeyDown(KeyCode.Keypad5)) IntroChar("5");
            else if(Input.GetKeyDown(KeyCode.Keypad6)) IntroChar("6");
            else if(Input.GetKeyDown(KeyCode.Keypad7)) IntroChar("7");
            else if(Input.GetKeyDown(KeyCode.Keypad8)) IntroChar("8");
            else if(Input.GetKeyDown(KeyCode.Keypad9)) IntroChar("9");
        
    } 

    void IntroChar(string Char)
    {  
        text.GetComponent<TextMesh>().text = string.Concat(text.GetComponent<TextMesh>().text, Char);
        GameManager.gameManager.newGameName = text.GetComponent<TextMesh>().text;
    }
}
