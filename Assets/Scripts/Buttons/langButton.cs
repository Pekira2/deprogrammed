﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class langButton : MonoBehaviour
{

    public string langId;
    public Transform langMark;

    void Start()
    {  
        if (GameManager.gameManager.language == langId) MoveTextMark();
    } 

    void OnMouseDown()
    {  
            GetComponent<AudioSource>().Play();
            SaveLanguage();
            GameManager.gameManager.Load();
            MoveTextMark();
    }   

    void SaveLanguage()
    {  
        foreach (UserPreference userPreference in GameManager.gameManager.userPreferencesCollection.userPreferences) {
            if (userPreference.id=="language") userPreference.value = langId;
        }
        GameManager.gameManager.SaveUserPreferences();
    } 

    void MoveTextMark()
    {  
        langMark.position = new Vector3(transform.position.x+5.0f, transform.position.y, langMark.position.z);
    } 

}
