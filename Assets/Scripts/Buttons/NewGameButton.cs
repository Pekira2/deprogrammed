﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class NewGameButton : MonoBehaviour
{
    void OnMouseDown()
    {  
            SceneManager.LoadScene("0NEWGAME");
            GetComponent<AudioSource>().Play();
    }    
}
