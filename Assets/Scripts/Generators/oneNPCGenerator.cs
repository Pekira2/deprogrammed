﻿using UnityEngine;
using System.Collections;

public class oneNPCGenerator : MonoBehaviour {

	
	public GameObject[] npcArray;
    GameObject cloneObj;
	
	void Start () {
		Generate();
	}
	
	
	void Generate () {
		if (!GameManager.gameManager.pause) {
			cloneObj = (GameObject)  Instantiate(npcArray[Random.Range(0, npcArray.Length)], transform.position, Quaternion.identity) as GameObject;        		
    	}
	}
}
