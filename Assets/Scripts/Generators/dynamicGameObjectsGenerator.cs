using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class dynamicGameObjectsGenerator : MonoBehaviour {

	public float cellWidth = 60;
	public float cellHigh = 60;

	public int numCellsX = 80;
	public int numCellsY = 20;


	public float playerRadius = 0.5f;
    public LayerMask whatIsPlayer;
    float xIni;
	float yIni;
	float xEnd;
	float yEnd;
	public GameObject cellPrefab;
	public GameObject borderPrefab;
	public GameObject blackBlockPrefab;
	public GameObject linePrefab;
	public GameObject blockPrefab;
	public GameObject wallPrefab;
	public GameObject groundPrefab;
	public GameObject muellePrefab;
	public GameObject elevatorPrefab;
	public GameObject hPlatformPrefab;
	public GameObject NPCPrefab;	
	public GameObject posTextPrefab;
	public GameObject flipEdgePrefab;
	public GameObject jumpEdgeRightPrefab;
	public GameObject jumpEdgeLeftPrefab;	
	public GameObject playerPrefab;
	public GameObject enemyPrefab;
	public GameObject characterInfoPrefab;


	private GameObject cam;
    private GameObject player;
    private GameObject enemy;

	GameObject clonObject;
	GameObject clonObject2;
	
	void Start () {
		Initialize();
		GenerateCoord();
		GenerateStruc();
	}
	
	void Update () {
	
	}

	void Initialize () {
		cam = GameObject.FindGameObjectWithTag("MainCamera");
		player = GameObject.FindGameObjectWithTag("Player");
		enemy = GameObject.FindGameObjectWithTag("Enemy");
	}

	void GenerateCoord () {
		xIni = -cellWidth*numCellsX/2-cellWidth;
		yIni = -cellHigh*numCellsY/2-cellHigh;
		xEnd = cellWidth*numCellsX/2+cellWidth;
		yEnd = cellHigh*numCellsY/2+cellHigh;
		float x=xIni;
		float y=yIni;
		do
       	{
           	do
        	{
        		if (x==xIni || x==xEnd-1 || y==yIni || y==yEnd-1)
        		{
        			clonObject = Instantiate(borderPrefab, new Vector3 (x, y, borderPrefab.transform.position.z), Quaternion.identity) as GameObject;
        		} else
        		{
        	    	clonObject = Instantiate(cellPrefab, new Vector3 (x, y, cellPrefab.transform.position.z), Quaternion.identity) as GameObject;
        	    }
        	    clonObject.transform.parent = transform;
        	    y+=cellHigh;
        	} while(y<yEnd);          
        	x+=cellWidth;
        	y=yIni;
        } while(x<xEnd);
	}


	void GenerateStruc () {
		
		float w = xEnd-xIni;
		float h = yEnd-yIni;


		string[] lines = new string[numCellsY*2];		
		//Debug.Log("numCellY="+numCellsY);

       foreach (SceneObj sceneObj in GameManager.gameManager.sceneCollection.Scenes) {

            if (sceneObj.sceneName == SceneManager.GetActiveScene().name)
            {
				lines[19]  = sceneObj.line19;
				lines[18]  = sceneObj.line18;
				lines[17]  = sceneObj.line17;
				lines[16]  = sceneObj.line16;
				lines[15]  = sceneObj.line15;
				lines[14]  = sceneObj.line14;
				lines[13]  = sceneObj.line13;
				lines[12]  = sceneObj.line12;
				lines[11]  = sceneObj.line11;
				lines[10]  = sceneObj.line10;	
				lines[9]  = sceneObj.line09;
				lines[8]  = sceneObj.line08;
				lines[7]  = sceneObj.line07;
				lines[6]  = sceneObj.line06;
				lines[5]  = sceneObj.line05;
				lines[4]  = sceneObj.line04;
				lines[3]  = sceneObj.line03;
				lines[2]  = sceneObj.line02;
				lines[1]  = sceneObj.line01;
				lines[0]  = sceneObj.line00;
		        break;
		    }
        }
		
		float x=xIni;
		float y=yIni;
		for (int i=0; i<numCellsX*2  ; i++)
       	{
           	for (int j=0; j<numCellsY*2 ; j++)
        	{
        		Debug.Log("i="+i+"   j="+j+ "       linea="+lines[j]);
        		drawGameObject(lines[j].Substring(i, 1), x, y);    	
        		y+=cellHigh/2;
        	}	
        	x+=cellWidth/2;
        	y=yIni;
        }
    


	}



	void drawGameObject (string simb, float x, float y) {
		if (simb == "-")
		{
			clonObject = (GameObject)  Instantiate(linePrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject; 
			//Debug.Log("x0"+x+"   y="+y);           		
        } else if (simb == "#")
		{
			clonObject = (GameObject)  Instantiate(blockPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;    		
        } else if (simb == "n")
		{
			clonObject = (GameObject)  Instantiate(blackBlockPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;    		
        } else if (simb == "g")
		{
			clonObject = (GameObject)  Instantiate(groundPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;    		
        } else if (simb == "|") {
        	clonObject = (GameObject)  Instantiate(wallPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject; 
        	clonObject = (GameObject)  Instantiate(flipEdgePrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;
        } else if (simb == "r") {
        	clonObject = (GameObject)  Instantiate(jumpEdgeRightPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;
        	clonObject = (GameObject)  Instantiate(linePrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject; 
        } else if (simb == "l") {
        	clonObject = (GameObject)  Instantiate(jumpEdgeLeftPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;
        	clonObject = (GameObject)  Instantiate(linePrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject; 
        
        } else if (simb == "p") {
        	clonObject = (GameObject)  Instantiate(playerPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;
			clonObject2 = (GameObject)  Instantiate(characterInfoPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z-1), Quaternion.identity) as GameObject;
			clonObject2.GetComponent<characterTextController>().character = clonObject;
        } else if (simb == "e") {
        	clonObject = (GameObject)  Instantiate(enemyPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z), Quaternion.identity) as GameObject;
			clonObject2 = (GameObject)  Instantiate(characterInfoPrefab, new Vector3 (x, y, GetComponent<Transform>().position.z-1), Quaternion.identity) as GameObject;
			clonObject2.GetComponent<characterTextController>().character = clonObject;
        }
	
	}
}
