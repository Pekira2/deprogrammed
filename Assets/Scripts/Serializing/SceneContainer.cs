using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
 
[XmlRoot("SceneCollection")]
public class SceneContainer
{
    [XmlArray("Scenes")]
  
    public SceneObj[] Scenes;
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
   
    public static SceneContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(SceneContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as SceneContainer;
        }
    }
}