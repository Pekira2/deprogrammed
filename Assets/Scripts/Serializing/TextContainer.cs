using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
 
[XmlRoot("TextCollection")]
public class TextContainer
{
    [XmlArray("Texts")]

    public TextObj[] Texts;
   
    public void Save(string path)
    {
        var serializer = new XmlSerializer(typeof(TextContainer));
        using(var stream = new FileStream(path, FileMode.Create))
        {
            serializer.Serialize(stream, this);
        }
    }
   
    public static TextContainer Load(string path)
    {
        var serializer = new XmlSerializer(typeof(TextContainer));
        using(var stream = new FileStream(path, FileMode.Open))
        {
            return serializer.Deserialize(stream) as TextContainer;
        }
    }
}