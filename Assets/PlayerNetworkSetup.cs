﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerNetworkSetup : NetworkBehaviour {

	// Use this for initialization
	public override void OnStartLocalPlayer () {
		GetComponent<playerController>().enabled = true;
	}
}
